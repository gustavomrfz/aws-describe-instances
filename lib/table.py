class Format(object):
    def __init__(self, selection):
        self.selection = selection

    def __str__(self):
        try:
            greatest_width={}
            field_width={}
            section_separator_width=0
            column_separator=" |  "
            column_separator_width = len(column_separator)

            for el in self.selection:
                instance = el.get()
                for title in instance.keys():
                    value = instance[title]
                    if value == "null":
                        print "is null"
                        raise
                    if 'greatest_width['+title+']' not in globals():
                        greatest_width[title]=len(title)

                    if len(value) > greatest_width[title]:
                        greatest_width[title] = len(value)

            columns = len(greatest_width)
            section_separator_width += columns * column_separator_width + columns / 2

            for k in greatest_width.keys():
                section_separator_width += greatest_width[k]
                field_width[k]=str(greatest_width[k])

            header=""
            tuples=""


            for k in field_width.keys():
                header=" |  " + "{0:^{width}}".format(k, width=field_width[k]) + header
            header += " |\n"

            table = '-' * section_separator_width + "\n"
            table = header + table
            table = '-' * section_separator_width + "\n" + table

            for el in self.selection:
                instance = el.get()
                for k in field_width.keys():
                     tuples = " |  " + "{0:<{width}}".format(instance[k], width=field_width[k]) + tuples
                tuples +=" |\n"
            table = table + tuples
            table = table + '-' * section_separator_width + "\n"
            return table
        except Exception as err:
            print "Could not format table" + err
            raise
