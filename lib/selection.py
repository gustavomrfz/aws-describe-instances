class Selection(object):
    def __init__(self, ec2_instance):
        self.instance = ec2_instance
        self.selection = {}

    def set(self, value):
        try:
            if value == "Instance Id":
                self.selection[value] = self.instance.get('InstanceId')
            elif value == "Public Ip Address":
                self.selection[value] = self.instance.get('PublicIpAddress')
            elif value == "State":
                self.selection[value] = self.instance.get('StateName')
            elif value == "Instance Name":
                self.selection[value] = self.instance.get('Name')
        except Exception as err:
            print "You must query for valid value" + err
            raise

    def get(self, value=""):
        if value == "":
            return self.selection
        else:
            return self.selection.get(value)

    def some_value_null():
        for k in self.selection.keys():
            if self.selection[k] is None:
                return True
