class Reservations(object):
    def __init__(self,ec2):
        self.reservations = ec2.describe_instances().get('Reservations',[{}])[:]

    def get(self):
        return self.reservations
