class EC2_instance(object):
    def __init__(self,l_instance):
        self.instance = l_instance.get('Instances',[{}])[0]

    def get(self,field):
        if field == "Name":
            tags = self.instance.get('Tags')[:]
            for pair in tags:
                for label in pair.keys():
                    if label == 'Key' and pair[label] == 'Name':
                        return pair['Value']
        elif field == "StateName":
            return self.instance.get('State').get('Name')
        else:
            return self.instance.get(field)
