import boto3
from lib.reservations import Reservations
from lib.ec2_instance import EC2_instance
from lib.selection import Selection
from lib.table import Format
from argparse import ArgumentParser

parser = ArgumentParser(description="Show fields of EC2 instances")

parser.add_argument('-f', '--field', action='append', default=[],
                     dest='fields', metavar='FIELD[s]',
                     help="fields of instance (separated by comas)\
                           Valid fields: [id,ip,status,name]")

ec2 = boto3.client('ec2')
reserv = Reservations(ec2)

opt = []

args = parser.parse_args().fields[0].split(",")
for arg in args:
    if arg == 'id':
        opt.append('Instance Id')
    if arg == 'ip':
        opt.append('Public Ip Address')
    if arg == 'state':
        opt.append('State')
    if arg == 'name':
        opt.append('Instance Name')

sel = []
n = 0
for el in reserv.get():
    instance = EC2_instance(el)
    sel.append(Selection(instance))
    for value in opt:
        sel[n].set(value)
    n += 1

print Format(sel)
